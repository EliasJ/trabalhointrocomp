import pygame

def desenhaCharmander(win, font, cor):
    text = font.render('Charmander', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 120)
    win.blit(text, textRect)

def desenhaBulbasaur(win, font, cor):
    text = font.render('Bulbasaur', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 192)
    win.blit(text, textRect)

def desenhaSquirtle(win, font, cor):
    text = font.render('Squirtle', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 264)
    win.blit(text, textRect)

def desenhaGengar(win, font, cor):
    text = font.render('Gengar', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 336)
    win.blit(text, textRect)

def desenhaRhydon(win, font, cor):
    text = font.render('Rhydon', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 408)
    win.blit(text, textRect)

def desenhaPikachu(win, font, cor):
    text = font.render('Pikachu', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 480)
    win.blit(text, textRect)

def desenhaDragonite(win, font, cor):
    text = font.render('Dragonite', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 552)
    win.blit(text, textRect)

def desenhaMewtwo(win, font, cor):
    text = font.render('Mewtwo', True, cor, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 624)
    win.blit(text, textRect)

def desenhaMenuInicial(win, player, pos):
    pygame.draw.rect(win, (255, 255, 255), (256, 72, 518, 648), 5)
    font = pygame.font.Font('fontes/joystix_monospace.ttf', 40)

    text = font.render('Choose your Pokémon', True, (255, 255, 255), (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (1024 // 2, 36)
    win.blit(text, textRect)

    desenhaCharmander(win, font, (255, 0, 0))
    desenhaBulbasaur(win, font, (0, 255, 0))
    desenhaSquirtle(win, font, (0, 0, 255))
    desenhaGengar(win, font, (97, 0, 80))
    desenhaRhydon(win, font, (215, 213, 216))
    desenhaPikachu(win, font, (255, 217, 0))
    desenhaDragonite(win, font, (187, 187, 255))
    desenhaMewtwo(win, font, (255, 167, 191))

def moveMenu(win, pos):
    pygame.draw.polygon(win, (255, 0, 0), ((280, 100 + pos*72), (320, 120 + pos*72), (280, 140 + pos*72)))

def selecionaPokemon(win, pos):
    font = pygame.font.Font('fontes/joystix_monospace.ttf', 40)

    if pos == 0:
        desenhaCharmander(win, font, (125, 0, 0))
    if pos == 1:
        desenhaBulbasaur(win, font, (0, 125, 0))
    if pos == 2:
        desenhaSquirtle(win, font, (0, 0, 125))
    if pos == 3:
        desenhaGengar(win, font, (45, 0, 40))
    if pos == 4:
        desenhaRhydon(win, font, (155, 153, 156))
    if pos == 5:
        desenhaPikachu(win, font, (135, 90, 0))
    if pos == 6:
        desenhaDragonite(win, font, (117, 117, 155))
    if pos == 7:
        desenhaMewtwo(win, font, (155, 105, 125))

    pygame.draw.polygon(win, (0, 255, 0), ((780, 100 + pos * 72), (820, 120 + pos * 72), (780, 140 + pos * 72)))