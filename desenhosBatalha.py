import pygame
import random
from pokemon import Pokemon

def backgroundBatalha(win, poke1, poke2):
    fundo = pygame.image.load('desenhos/FundoPokemon.png')
    fundo = pygame.transform.scale(fundo, (1024, 600))
    win.blit(fundo, (0, 0))

    barraMensagem = pygame.image.load('desenhos/text_bar.png')
    barraMensagem = pygame.transform.scale(barraMensagem, (1024, 168))
    win.blit(barraMensagem, (0, 600))

    menuBatalha = pygame.image.load('desenhos/fgt_options.png')
    menuBatalha = pygame.transform.scale(menuBatalha, (334, 168))
    win.blit(menuBatalha, (690, 600))

    vidaPoke1 = pygame.image.load('desenhos/barra_2.png')
    vidaPoke1 = pygame.transform.scale(vidaPoke1, (470, 160))
    win.blit(vidaPoke1, (540, 435))

    vidaPoke2 = pygame.image.load('desenhos/barra_1.png')
    vidaPoke2 = pygame.transform.scale(vidaPoke2, (510, 130))
    win.blit(vidaPoke2, (60, 75))

    poke1.nomePokemonEscolhido(win)
    poke2.nomePokemonAleatorio(win)

def desenhaSeta(win, posx, posy):
    seta = pygame.image.load('desenhos/le_setinha.png')
    seta = pygame.transform.scale(seta, (25, 25))
    win.blit(seta, (710 + posx*150, 650 + posy*50))

def pokemonAppeared(win, pokemon):
    font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
    msg = "Um " + pokemon.nome + " apareceu!"
    text = font.render(msg, True, (255, 255, 255), (40, 79, 102))
    win.blit(text, (50, 630))

def pokemonWhatToDo(win, pokemon):
    font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
    msg = "O que " + pokemon.nome + " fará?"
    text = font.render(msg, True, (255, 255, 255), (40, 79, 102))
    win.blit(text, (50, 630))

def fugaFalha(win):
    a = True

    while a:
        pygame.time.delay(50)
        font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
        msg = "Não conseguiu fugir!"
        text = font.render(msg, True, (255, 255, 255), (40, 79, 102))
        pygame.draw.rect(win, (40, 79, 102), ((50, 630), (500, 70)))
        win.blit(text, (50, 630))

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                a = False

        pygame.display.update()

def mensagemBatalha(win, msg):
    a = True
    i = 0
    font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
    text = ''

    for i in range(len(msg)):
        pygame.time.delay(50)

        text += msg[i]
        mensagem = font.render(text, True, (255, 255, 255), (40, 79, 102))
        pygame.draw.rect(win, (40, 79, 102), ((50, 630), (620, 70)))
        win.blit(mensagem, (50, 630))

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                break

        pygame.display.update()

    # while a:
    #     pygame.time.delay(50)
    #     font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
    #     text = font.render(msg, True, (255, 255, 255), (40, 79, 102))
    #     pygame.draw.rect(win, (40, 79, 102), ((50, 630), (620, 70)))
    #     win.blit(text, (50, 630))
    #
    #     for event in pygame.event.get():
    #         if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
    #             a = False
    #
    #     pygame.display.update()
    #     i += 1

    pygame.time.delay(500)

def setaLuta(win, posx, posy):
    pygame.draw.polygon(win, (50, 50, 50), ((35 + 228*posx, 648 + 60*posy), (50 + 228*posx, 658 + 60*posy), (35 + 228*posx, 668 + 60*posy)))

def lutar(win, pokemon):
    a = True
    posx, posy = 0, 0

    while a:
        pygame.time.delay(50)
        barraLuta = pygame.image.load('desenhos/pp_bar.png')
        barraLuta = pygame.transform.scale(barraLuta, (1024, 168))
        win.blit(barraLuta, (0, 600))
        pokemon.desenhaAtaques(win)
        setaLuta(win, posx, posy)
        pokemon.ataqueAtual(win, posx, posy)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                a = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN and posy < 1:
                    posy += 1
                elif event.key == pygame.K_UP and posy > 0:
                    posy -= 1
                elif event.key == pygame.K_LEFT and posx > 0:
                    posx -= 1
                elif event.key == pygame.K_RIGHT and posx < 1:
                    posx += 1
                elif event.key == pygame.K_RETURN:
                    return pokemon.ataqueAtual(win, posx, posy)

        pygame.display.update()


def menuDireitoInferior(win, poke1, poke2, posx, posy):
    if posx == 1 and posy == 1:
        fugir = random.randint(0, 2)
        if fugir == 0:
            exit()
        else:
            mensagemBatalha(win, "Não conseguiu fugir!")
    elif posx == 0 and posy == 0:
        return lutar(win, poke1)

    return None