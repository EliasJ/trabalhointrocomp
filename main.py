import pygame
from pygame import mixer
import menuInicial as mi
import random
import desenhosBatalha as db
from pokemon import Pokemon
from ataque import Ataque

#normal
tackle = Ataque("Investida", 30, 0, 35)
qAttack = Ataque("Ataque Rápido", 40, 0, 30)
corteDuplo = Ataque("Corte", 40, 0, 25)
lamber = Ataque("Lamber", 15, 0, 50)
pisao = Ataque("Pisão", 35, 0, 40)

#fogo
ember = Ataque("Brasas", 50, 0, 20)
explosaoDeChamas = Ataque("Chamas", 60, 0, 15)

#grama
absorb = Ataque("Absorve", 30, 15, 30)
folhasDeNavalha = Ataque("Navalha", 40, 0, 20)

#água
bolhas = Ataque("Bolhas", 35, 0, 20)
hidroBomba = Ataque("Hidro Bomba", 45, 0, 15)

#psiquico
hipnose = Ataque("Hipnose", 40, 0, 25)
darkPulse = Ataque("Vibração Sombria", 50, 0, 20)
confusao = Ataque("Confusão", 40, 0, 25)
regenerar = Ataque("Regenerar", 0, 50, 10)

#dark
bite = Ataque("Mordida", 40, 0, 25)

#raio
choqueDoTrovao = Ataque("C. do Trovão", 50, 0, 25)
raio = Ataque("Raio", 40, 0, 20)

#terra
megaChifre = Ataque("Mega Chifre", 60, 0, 15)

#vento
ciclone = Ataque("Ciclone", 50, 0, 20)

pokemons = [["Charmander",[tackle, qAttack, ember, explosaoDeChamas], 250],
            ["Bulbasaur", [tackle, qAttack, absorb, folhasDeNavalha], 350],
            ["Squirtle", [tackle, qAttack, bolhas, hidroBomba], 300],
            ["Gengar", [corteDuplo, lamber, hipnose, darkPulse], 200],
            ["Rhydon", [tackle, bite, pisao, megaChifre], 600],
            ["Pikachu", [tackle, qAttack, raio, choqueDoTrovao], 400],
            ["Dragonite", [pisao, regenerar, explosaoDeChamas, ciclone], 750],
            ["Mewtwo", [confusao, qAttack, regenerar, darkPulse], 900]]

pygame.init()

#musicaInicial = pygame.mixer.music.load('musicas/fireRedAbertura.mp3')
#mixer.music.load('musicas/firered_0005.wav')
#pygame.mixer.music.play()

somMoveSeta = pygame.mixer.Sound('musicas/firered_0005.wav')
somConfirmacao = pygame.mixer.Sound('musicas/firered_0030.wav')
music = pygame.mixer.music.load('musicas/audio.mp3')
pygame.mixer.music.play(-1)

win = pygame.display.set_mode((1024,768))
pygame.display.set_caption('PokePy')

posicaoInicial = 0

run = True

menu = True
batalha = False
player = False
posPlayer = -1

while menu:

    pygame.time.delay(10)

    win.fill((0, 0, 0))
    mi.desenhaMenuInicial(win, player, posPlayer)
    mi.moveMenu(win, posicaoInicial)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            menu = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN and posicaoInicial < 7:
                posicaoInicial += 1
                somMoveSeta.play()
            elif event.key == pygame.K_UP and posicaoInicial > 0:
                posicaoInicial -= 1
                somMoveSeta.play()
            elif event.key == pygame.K_RETURN:
                somConfirmacao.play()
                posPlayer = posicaoInicial
                adversario = posPlayer

                while adversario == posPlayer:
                    adversario = random.randint(0, 7)

                poke1 = Pokemon(True, posPlayer, pokemons[posPlayer])
                poke2 = Pokemon(False, adversario, pokemons[adversario])
                menu = False
                batalha = True

    pygame.display.update()

posx, posy = 0, 0
ready = False
tempo = 0
rodada = True

win.fill((0, 0, 0))
pygame.display.update()
music = pygame.mixer.music.load('musicas/batalha.mp3')
pygame.mixer.music.play(-1)
#pygame.time.delay(3000)

while batalha == True:
    db.backgroundBatalha(win, poke1, poke2)
    db.desenhaSeta(win, posx, posy)

    poke1.desenhaVidaPokemon(win)
    poke1.desenhaBarraVida(win)
    poke1.desenhaPokemon(win)
    poke2.desenhaPokemon(win)
    poke2.desenhaBarraVida(win)

    #pygame.draw.line(win, (255, 0, 0), (757, 513), (973, 513), 1)
    #pygame.draw.line(win, (255, 0, 0), (757, 513), (757, 525))

    #pygame.draw.line(win, (255, 0, 0), (261, 147), (504, 147), 1)
    #pygame.draw.line(win, (255, 0, 0), (261, 147), (260, 160), 1)

    pygame.time.delay(50)

    if tempo == 0:
        db.pokemonAppeared(win, poke2)

    if rodada == True:
        if tempo > 0:
            db.pokemonWhatToDo(win, poke1)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                batalha = False
            if event.type == pygame.KEYDOWN:
                tempo += 1
                if event.key == pygame.K_DOWN and posy < 1:
                    somMoveSeta.play()
                    posy += 1
                elif event.key == pygame.K_UP and posy > 0:
                    somMoveSeta.play()
                    posy -= 1
                elif event.key == pygame.K_LEFT and posx > 0:
                    somMoveSeta.play()
                    posx -= 1
                elif event.key == pygame.K_RIGHT and posx < 1:
                    somMoveSeta.play()
                    posx += 1
                elif event.key == pygame.K_RETURN:
                    somConfirmacao.play()
                    atk = db.menuDireitoInferior(win, poke1, poke2, posx, posy)
                    if atk != None:
                        poke1.ataca(atk, poke2)
                        poke1.usouAtaque(win, atk)
                    rodada = False

    else:
        pygame.time.delay(1000)
        atk = poke2.ataquePvE()
        poke2.ataca(atk, poke1)
        poke2.usouAtaque(win, atk)
        rodada = True

    if poke1.morto():
        poke1.desenhaBarraVida(win)
        poke1.desenhaVidaPokemon(win)
        db.mensagemBatalha(win, poke1.nome + " morreu!")
        db.mensagemBatalha(win, poke2.nome + " venceu!")
        break
    elif poke2.morto():
        poke2.desenhaBarraVida(win)
        db.mensagemBatalha(win, poke2.nome + " morreu!")
        db.mensagemBatalha(win, poke1.nome + " venceu!")
        break

    pygame.display.update()

pygame.time.delay(250)
