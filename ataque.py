import pygame

class Ataque:

    def __init__(self, nome, dano, hp, qtd):
        self.nome = nome
        self.dano = dano
        self.hpRegen = hp
        self.cargaTotal = qtd
        self.cargaRestante = qtd

    def mensagemAtaque(self, win, nomePokemon):
        a = True

        while a:
            pygame.time.delay(50)
            font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)
            msg = "Não conseguiu fugir!"
            text = font.render(msg, True, (255, 255, 255), (40, 79, 102))
            pygame.draw.rect(win, (40, 79, 102), ((50, 630), (600, 70)))
            win.blit(text, (50, 630))

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                    a = False

            pygame.display.update()


