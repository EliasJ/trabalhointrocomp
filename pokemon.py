import pygame
import random
import desenhosBatalha as db
from ataque import Ataque

class Pokemon:
    def __init__(self, escolha, pos, caracteristicas):
        self.amigo = escolha
        self.nome = caracteristicas[0]
        self.ataques = caracteristicas[1]
        self.hp = caracteristicas[2]
        self.hpmax = caracteristicas[2]
        self.pos = pos
        self.costas, self.frente = self.imagens(pos)

    def desenhaPokemon(self, win):
        if self.amigo:
            self.costas = pygame.transform.scale(self.costas, (256, 256))
            if self.pos == 0:
                win.blit(self.costas, (110, 385))
            elif self.pos == 1:
                win.blit(self.costas, (125, 408))
            elif self.pos == 2:
                win.blit(self.costas, (125, 405))
            elif self.pos == 3:
                win.blit(self.costas, (125, 385))
            elif self.pos == 4:
                win.blit(self.costas, (125, 355))
            elif self.pos == 5:
                win.blit(self.costas, (125, 370))
            elif self.pos == 6:
                win.blit(self.costas, (125, 370))
            elif self.pos == 7:
                win.blit(self.costas, (125, 350))
        else:
            self.frente = pygame.transform.scale(self.frente, (256, 256))
            if self.pos == 0:
                win.blit(self.frente, (640, 140))
            elif self.pos == 1:
                win.blit(self.frente, (620, 160))
            elif self.pos == 2:
                win.blit(self.frente, (610, 140))
            elif self.pos == 3:
                win.blit(self.frente, (630, 140))
            elif self.pos == 4:
                win.blit(self.frente, (620, 120))
            elif self.pos == 5:
                win.blit(self.frente, (620, 140))
            elif self.pos == 6:
                win.blit(self.frente, (620, 130))
            elif self.pos == 7:
                win.blit(self.frente, (590, 120))

    def imagens(self, pos):
        if pos == 0:
            return pygame.image.load('desenhos/costas/4.png'), pygame.image.load('desenhos/frente/4.png')
        elif pos == 1:
            return pygame.image.load('desenhos/costas/1.png'), pygame.image.load('desenhos/frente/1.png')
        elif pos == 2:
            return pygame.image.load('desenhos/costas/7.png'), pygame.image.load('desenhos/frente/7.png')
        elif pos == 3:
            return pygame.image.load('desenhos/costas/94.png'), pygame.image.load('desenhos/frente/94.png')
        elif pos == 4:
            return pygame.image.load('desenhos/costas/112.png'), pygame.image.load('desenhos/frente/112.png')
        elif pos == 5:
            return pygame.image.load('desenhos/costas/25.png'), pygame.image.load('desenhos/frente/25.png')
        elif pos == 6:
            return pygame.image.load('desenhos/costas/149.png'), pygame.image.load('desenhos/frente/149.png')
        elif pos == 7:
            return pygame.image.load('desenhos/costas/150.png'), pygame.image.load('desenhos/frente/150.png')

    def desenhaAtaques(self, win):
        font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)

        text = font.render(self.ataques[0].nome, True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (55, 640))

        text = font.render(self.ataques[1].nome, True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (290, 640))

        text = font.render(self.ataques[2].nome, True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (55, 700))

        text = font.render(self.ataques[3].nome, True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (290, 700))

    def ataqueAtual(self, win, posx, posy):
        ataque = 0
        if posx == 0 and posy == 0:
            ataque = self.ataques[0]
        elif posx == 1 and posy == 0:
            ataque = self.ataques[1]
        elif posx == 0 and posy == 1:
            ataque = self.ataques[2]
        elif posx == 1 and posy == 1:
            ataque = self.ataques[3]

        font = pygame.font.Font('fontes/joystix_monospace.ttf', 36)

        text = font.render(str(self.ataques[0].cargaRestante), True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (848, 635))

        text = font.render(str(self.ataques[0].cargaTotal), True, (50, 50, 50), (255, 255, 255))
        win.blit(text, (930, 635))

        return ataque

    def ataca(self, ataque, poke2):
        poke2.hp -= ataque.dano
        ataque.cargaRestante -= 1
        if self.hp + ataque.hpRegen >= self.hpmax:
            self.hp = self.hpmax
        else:
            self.hp += ataque.hpRegen

    def ataquePvE(self):
        return self.ataques[random.randint(0, 3)]

    def usouAtaque(self, win, ataque):
        barraMensagem = pygame.image.load('desenhos/text_bar.png')
        barraMensagem = pygame.transform.scale(barraMensagem, (1024, 168))
        win.blit(barraMensagem, (0, 600))
        db.mensagemBatalha(win, self.nome + " usou " + ataque.nome + "!")

    def morto(self):
        return self.hp <= 0

    def nomePokemonEscolhido(self, win):
        font = pygame.font.Font('fontes/joystix_monospace.ttf', 32)
        text = font.render(self.nome, True, (64, 64, 64), (247, 247, 218))
        win.blit(text, (605, 460))

        text = font.render("X", True, (64, 64, 64), (247, 247, 218))
        win.blit(text, (950, 460))

    def nomePokemonAleatorio(self, win):
        font = pygame.font.Font('fontes/joystix_monospace.ttf', 38)
        text = font.render(self.nome, True, (64, 64, 64), (247, 247, 218))
        win.blit(text, (95, 87))

        text = font.render("X", True, (64, 64, 64), (247, 247, 218))
        win.blit(text, (478, 87))

    def desenhaVidaPokemon(self, win):
        font = pygame.font.Font('fontes/joystix_monospace.ttf', 28)

        if self.hp > 0:
            text = font.render(str(self.hp) + "/" + str(self.hpmax), True, (64, 64, 64), (247, 247, 218))
            win.blit(text, (815, 534))
        else:
            text = font.render("0/" + str(self.hpmax), True, (64, 64, 64), (247, 247, 218))
            win.blit(text, (815, 534))

    def desenhaBarraVida(self, win):
        # x ini barra de vida poke1 = 757
        # x final barra de vida poke1 = 974
        # y ini barra de vida poke1 = 513
        # y final barra de vida poke1 = 526
        if self.amigo:
            coef = self.hp/self.hpmax*216
            if self.hp > 0:
                barraSemVida = pygame.image.load('desenhos/barra_sem_vida.png')
                barraSemVida = pygame.transform.scale(barraSemVida, (int(217 - coef), 13))
                win.blit(barraSemVida, (757 + coef, 513))
            else:
                barraSemVida = pygame.image.load('desenhos/barra_sem_vida.png')
                barraSemVida = pygame.transform.scale(barraSemVida, (217, 13))
                win.blit(barraSemVida, (757, 513))

            if 0.2 < self.hp/self.hpmax < 0.5:
                barraAmarela = pygame.image.load('desenhos/vida_amarela.png')
                barraAmarela = pygame.transform.scale(barraAmarela, (int(coef), 13))
                win.blit(barraAmarela, (757, 513))

            elif 0 < self.hp/self.hpmax <= 0.2:
                barraVermelha = pygame.image.load('desenhos/vida_vermelha.png')
                barraVermelha = pygame.transform.scale(barraVermelha, (int(coef), 13))
                win.blit(barraVermelha, (757, 513))


        # x ini barra de vida poke1 = 261
        # x final barra de vida poke1 = 504
        # y ini barra de vida poke1 = 147
        # y final barra de vida poke1 = 160
        else:
            coef = self.hp/self.hpmax*243
            barraSemVida = pygame.image.load('desenhos/barra_sem_vida.png')
            barraSemVida = pygame.transform.scale(barraSemVida, (int(244 - coef), 14))
            win.blit(barraSemVida, (261 + coef, 147))

            if 0.2 < self.hp/self.hpmax < 0.5:
                barraAmarela = pygame.image.load('desenhos/vida_amarela.png')
                barraAmarela = pygame.transform.scale(barraAmarela, (int(coef), 14))
                win.blit(barraAmarela, (261, 147))

            elif self.hp/self.hpmax <= 0.2:
                barraVermelha = pygame.image.load('desenhos/vida_vermelha.png')
                barraVermelha = pygame.transform.scale(barraVermelha, (int(coef), 14))
                win.blit(barraVermelha, (261, 147))

        #fazer para poke2
